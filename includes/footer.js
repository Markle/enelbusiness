  //Form activation
  var onloadCallback = function() {
    grecaptcha.render('rcaptcha_form_lead_a_caldo', {
      'callback': formValidation,
      'sitekey' : '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'   //this site key is for testing change with your own
    });
    // console.log('captcha loaded');
  };

  var formValidation = function(val) {
    // console.log(val);
    submitbutton.setAttribute('approved', 'true');
    changeButtonStete();
  };

  const checkAcconsento =  document.getElementById('acconsento');
  const checkNonAcconsento =  document.getElementById('nonacconsento');
  checkAcconsento.addEventListener('change', changeButtonStete);
  checkNonAcconsento.addEventListener('change', changeButtonStete);

  function changeButtonStete(){
    if(submitbutton.getAttribute('approved')){
      if(document.getElementById('acconsento').checked) {
        submitbutton.disabled = false;
      }
      if(document.getElementById('nonacconsento').checked) {
        submitbutton.disabled = true;
      }
    } else {
      submitbutton.disabled = true;
    }
  }

    //slide
    let slides = document.querySelectorAll('#slider .slide');
    let currentSlide = 0;

    if(slides.length > 1){
      let slideInterval = setInterval(nextSlide,5000);
    }
    function nextSlide(){
      slides[currentSlide].className = 'slide';
      currentSlide = (currentSlide+1)%slides.length;
      slides[currentSlide].className = 'slide showing';
    }
    const formAnchor = document.getElementById('anchor-form');
    const toFormButton = document.getElementsByClassName('anchor-richiesta');
    for (var i = 0; i < toFormButton.length; i++) {
      toFormButton.item(i).addEventListener('click', event => {
        scrollingtotop(formAnchor);
      });
    }

    // Animation scroll
    function scrollingtotop(el){
      el.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
      });
    }


    // slider
    const sliders = document.getElementById('carousel-container');
    const leftarrow = document.getElementById('leftarrow');
    const rightarrow = document.getElementById('rightarrow');

    //var zz = document.getElementById('slide-2').style.left - document.getElementById('slide-1').style.left ;
    leftarrow.addEventListener('click', event => {
      var scrollPos = sliders.scrollX || sliders.scrollLeft;
      var slideWidth = sliders.children[0].offsetWidth;

      let currentbox = document.getElementsByClassName('boxactive');
      let currentel = currentbox[0];
      if ("slide-1" != currentel.getAttribute('id')) {
        sliders.scrollTo({
          top: 0,
          left: scrollPos - slideWidth,
          behavior: 'smooth'
        });

        currentbox[0].classList.remove('boxactive');
        currentel.previousElementSibling.classList.add('boxactive');
      }
    });
    rightarrow.addEventListener('click', event => {
      var scrollPos = sliders.scrollX || sliders.scrollLeft;
      var slideWidth = sliders.children[0].offsetWidth;

      let currentbox = document.getElementsByClassName('boxactive');
      let currentel = currentbox[0];
      if ("slide-2" != currentel.getAttribute('id')) {
        sliders.scrollTo({
          top: 0,
          left: scrollPos + slideWidth,
          behavior: 'smooth'
        });
        currentbox[0].classList.remove('boxactive');
        currentel.nextElementSibling.classList.add('boxactive');
      }
    });
